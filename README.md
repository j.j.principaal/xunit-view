# xunit-view
Merges multiple test results into one html page

## Installation
(Requires yarn)

run `yarn`

## Usage
* Put the test output `.xml` files into the tests folder
* run `yarn use`